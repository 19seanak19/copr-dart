Name:           dart
Version:        2.18.6
Release:        1%{?dist}
Summary:        Dart SDK

License:        BSD
URL:            https://dart.dev

Source0:        https://storage.googleapis.com/dart-archive/channels/stable/release/%{version}/sdk/dartsdk-linux-x64-release.zip
Source1:        https://storage.googleapis.com/dart-archive/channels/stable/release/%{version}/sdk/dartsdk-linux-ia32-release.zip
Source2:        https://storage.googleapis.com/dart-archive/channels/stable/release/%{version}/sdk/dartsdk-linux-arm64-release.zip
Source3:        https://storage.googleapis.com/dart-archive/channels/stable/release/%{version}/sdk/dartsdk-linux-arm-release.zip

%global         SHA256SUM0 492c0e835203c4402e3d8291d12b53927f0300c8080aaf63a9113c204255a735
%global         SHA256SUM1 c97d1b3bf549690fb8c39392dd36b004795c54e808fe6c3503aa872850f79c32
%global         SHA256SUM2 61dbb462b48aee4f3184b6ecdd356632f39165ae8570fe77a62900a6444f702c
%global         SHA256SUM3 c360ff64d1306ed5038a7df98142eff832abebc486d3e2b7f68b0fc53279a849

ExcludeArch:    s390x ppc64

BuildRequires:  curl unzip
Requires:       glibc

%description
Dart SDK libraries and command-line tools for building Dart applications.
This is an unofficial package.

%global debug_package %{nil}
%define _build_id_links none

%prep
echo 'Prep for %{_arch}'
%ifarch x86_64
echo "%SHA256SUM0  %SOURCE0" | sha256sum -c -
%autosetup -T -b 0 -n dart-sdk
%endif
%ifarch %{ix86}
echo "%SHA256SUM1  %SOURCE1" | sha256sum -c -
%autosetup -T -b 1 -n dart-sdk
%endif
%ifarch aarch64
echo "%SHA256SUM2  %SOURCE2" | sha256sum -c -
%autosetup -T -b 2 -n dart-sdk
%endif
%ifarch %{arm}
echo "%SHA256SUM3  %SOURCE3" | sha256sum -c -
%autosetup -T -b 3 -n dart-sdk
%endif

%install
mkdir -p %{buildroot}%{_libdir}/dart
cp -r . %{buildroot}%{_libdir}/dart

%check
echo -e 'void main() {\n  print(r"Hello from Dart");\n}' >> hello_test.dart
%{buildroot}%{_libdir}/dart/bin/dart hello_test.dart
rm hello_test.dart

%post
%{__ln_s} -f %{_libdir}/dart/bin/dart %{_bindir}/dart

%postun
case "$1" in
    0)
        rm -f %{_bindir}/dart
    ;;
esac

%files
%{_libdir}/dart
%license LICENSE
%doc README

%changelog
* Fri Jan 6 2023 Sean Kimball - 2.18.6-1
- Update to Dart 2.18.6
* Thu Nov 24 2022 Sean Kimball - 2.18.5-1
- Update to Dart 2.18.5
* Sun Nov 20 2022 Sean Kimball - 2.18.4-1
- Initial package creation
