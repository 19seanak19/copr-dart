# copr-dart

This repo contains what's required for a Dart SDK rpm, hosted on Copr. 

This is an unofficial packaging, so please do not bother the Dart developers with any issues from using this package.

## Installing

```
dnf copr enable 19seanak19/dart
dnf install dart
```

## Uninstalling

```
dnf uninstall dart
dnf copr disable 19seanak19/dart
```

## Updating the package

This is more here to help me update, but this is how to update to a new Dart version.

1. Update the "Version" field in `dart.spec`
2. Update each SHA256 global in `dart.spec`
3. Update the "DART_VERSION" variable in `.copr/Makefile`